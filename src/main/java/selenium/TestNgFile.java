package selenium;

import org.testng.annotations.Test;

public class TestNgFile {
	

	@Test(priority=0)   //parametru priority
	public static void one() {
		System.out.println("First");
	}
	
	@Test(priority=1) 
	public static void two() {
		System.out.println("Second");
		
		}	
	
	@Test(priority=2) 
	public static void three() {
		System.out.println("Third");

	}
	

	@Test
	public static void four() {
		System.out.println("Forth");

	}
	

	@Test
	public static void five() {
		System.out.println("Fifth");

	}
	
	
}

//TestNG ruleaza in ordine alfabetica by default
